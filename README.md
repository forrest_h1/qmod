# qmod

qmod (formerly acmod.sh) is a simple tool I designed to help myself remember Linux permission codes. AT THIS STAGE ONLY NUMERICAL PERMISSION CODES ARE SUPPORTED - these are what I usually struggled with anyways. But alphabetical codes will be added in the future :)

## Usage

qmod is pretty straight-forward. Upon cloning, run `chmod u+x qmod.sh` to set the script to executable. After that, you'll be able to input any numerical permission code as an argument to get a detailed explanation about what each bit-value in each bit-position means.

E.g. `./qmod.sh 777` will explain what running `chmod 777` grants permission-wise.

Running a 2nd argument, `-i`, directly AFTER the numerical permission code will print a standard explanation of ALL bit-values and positions. 

E.g. `./qmod.sh 777 -i` will explain what running `chmod 777` will grant in terms of permissions, in addition to explaining to you what every numerical value in each permission grants you.

Since this is intended to be a cheatsheet for beginners:

As permission codes don't exceed 3 characters in length, this script will only take codes like `055` or `777`. Likewise, the maximum bit-value in each of the 3 positions cannot exceed 7 or go below 0. A full list of valid characters is available from the script. 

## TO-DO

- Add alpha character support
- Create neat ASCII thumbnail for the project