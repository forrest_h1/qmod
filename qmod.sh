#!/bin/bash
#Written by Forrest Hooker, 02/01/2023
#Quick cheatsheet written by me to remember all the chmod possibilities. Accepts
#numerical arguments only; for example, if I type ./acmod.sh 777, it will notify
#me of what each bit actually means.

## Arg Vars ##

#First argument, user must input numerical code
DIGIT_CODE=$1

#Second argument, in future will silence full explanation printed at the end of the script's output
FULL_EXPLAN=$2

## Arrs/Vars ##

#Length of ${DIGIT_CODE} User Argument 
DIGIT_CODE_LENGTH=$(echo ${DIGIT_CODE} | awk '{print length}')

#Split user input into array of characters
CODE_ARR=( $(echo ${DIGIT_CODE} | grep -o .) )

#Array of which bits in a permission code correlate to which permissions.
BIT_DESCRIPTORS=(
	["0"]="Sticky bit: this bit is a STICKY bit. This does not affect individual files, but DOES affect directory file deletion."
	["1"]="Owner bit: this bit declares what the OWNER is able to do with the file."
	["2"]="Group bit: this bit declares what the GROUP is able to do with the file."
	["3"]="World bit: this bit decares what anyone on the system is able to do with the file."
)

#Associative array of what each bit means in context of permissions
BITS=(
	["0"]="No Permissions for entity assigned to bit"
	["1"]="Execute only for entity assigned to bit"
	["2"]="Write only for entity assigned to bit"
	["3"]="Write & Execute (1+2) for entity assigned to bit"
	["4"]="Read only for entity assigned to bit"
	["5"]="Read & Execute (4+1) for entity assigned to bit"
	["6"]="Read & Write (5+1) for entity assigned to bit"
	["7"]="Read, Write, & Execute (4+2+1) for entity assigned to bit"
)

#Stupid array to mess with user
BAD_WORDS=("Fuck" "fuck" "Shit" "Damn" "Ass")

## Printf Vars ##

#Resets terminal to default
RESET=("\033[0m")
#Prints in bold 
BOLD=("\033[1m")
#Prints with underline below text
ULINE=("\033[21m")
#Prints in Green
GREEN=("\033[32m")
#Prints in Red
RED=("\033[31m")
#Prints in Yellow
YELLOW=("\033[33m")
## Functions ##

__MAIN__(){
	#Error checking for user argument given
	__ARG_CHECK__
	#Print explanation of what user has given
	__BIT_CHECK__
	#(Change this) If 2nd user arg is NOT empty, then...
	if ! [ -z ${FULL_EXPLAN} ]; then
		#Print full explanation
		__FULL_EXPLANATION__
	else
		printf "${GREEN}For a full explanation of bit position & values, run this command with <-i> as the 2nd argument.${RESET}\n"
		printf "\nex: ${BOLD}./qmod.sh 777 -i\n${RESET}"
	fi
	__EXIT__
}

__ARG_CHECK__(){
	#If user input is empty, then..
	if [ -z ${DIGIT_CODE} ]; then
		#Notify user
		printf "${BOLD}${RED}\nNo permission code given.\n${RESET}"
		#Print usage
		__USAGE__
	#Else if any part of ${DIGIT_CODE} or ${FULL_EXPLAN} matches the array of swearing, then...
	elif [[ ${BAD_WORDS[*]} =~ ${DIGIT_CODE} ]]; then
		#Mess with user
		printf "\nYeah? Well up yours too, buddy!\n"
		#Exit script
		__EXIT__
	#Else if any part of ${CODE_ARR[*]} is not a digit, then...
	elif [[ ! ${DIGIT_CODE} == ?(-)+([0-9]) ]]; then
		#Notify user
		printf "${BOLD}${RED}\nCode given is either out of range or not supported.\n${RESET}"
		#Print usage
		__USAGE__
	#Else, if the length of ${DIGIT_CODE} is not 3...
	elif [[ ${DIGIT_CODE_LENGTH} != 3 ]]; then
		#If ${DIGIT_CODE_LENGTH} is LESS than 3, then...
	       	if [[ ${DIGIT_CODE_LENGTH} < 3 ]]; then
			#Notify the user.
		      	printf "${BOLD}${RED}\nPermission code given isn't long enough. Try a 3-digit or 4-digit code.\n${RESET}"
		      	#Print usage.
			__USAGE__
		#Else, if ${DIGIT_CODE_LENGTH} is MORE than 4, then..
		elif [[ ${DIGIT_CODE_LENGTH} > 4 ]]; then
			#Notify the user.
			printf "${BOLD}${RED}\nPermission code is too long. Try a 3-digit or 4-digit code.\n${RESET}"
			#Print usage.
			__USAGE__
		fi	#End inner if-statement
	#Else, if none of these (argument has passed arg check)...
	else
		#For each element (Character/digit/number) of the user argument, do...
		for num in ${CODE_ARR[@]}; do
			#If the counted element is NOT Greater Than/Equal To 0 AND NOT Less Than/Equal To 7, then... 
			if ! [[ ${num} -ge 0 && ${num} -le 7 ]]; then
				#Notify the user.
				printf "${BOLD}${RED}Invalid bit given. Please use [0-7].\n${RESET}"
				#Print usage.
				__USAGE__
			fi 	#End inner if-statement
		done	#End For loop
	fi	#End Main if.
}

#Reads the arg-checked values given by the user and gives descriptions of each, bit by bit. 
__BIT_CHECK__(){
	if [ ${DIGIT_CODE_LENGTH} = 3 ]; then
		#For each index of the value position of the values in ${BIT_DESCRIPTORS}, do...
		for octet in "${!BIT_DESCRIPTORS[@]}"; do
			#Print the normal values of the current ${BIT_DESCRIPTORS[@]} value in bold...
			printf "${BOLD}\n${BIT_DESCRIPTORS[$octet+1]}\n${RESET}"
			#For each number given in ${CODE_ARR} with an index of the current counter, do...
			for number in ${CODE_ARR[${octet}]}; do
				#Print the user number given as well as the description from ${BITS[@]} found by using $number as an index
				printf "${number} - ${BITS[${number}]}\n\n"
				#Break from the loop since we only need a single number to fit under the header
				break	
			done 	#End inner loop
		done 	#End outer loop
	else
		for octet in "${!BIT_DESCRIPTORS[@]}"; do
			printf "${BOLD}\n${BIT_DESCRIPTORS[$octet]}\n${RESET}"
			for number in ${CODE_ARR[${octet}]}; do
				printf "${number} - ${BITS[${number}]}\n\n"
				break
			done
		done
	fi
}


## Sub Functions ##

#Prints full explanation of chmod permissions
__FULL_EXPLANATION__(){
	printf "Example: 777\n\n"
	printf "7            7            7\n\n"
	printf "^            ^            ^\n"
	printf "|            |            |\n"
	printf "|            |            ${ULINE}World${RESET}\n"
    	printf "|            ${ULINE}Group${RESET}	  \n"
	printf "${ULINE}Owner${RESET}                     \n\n"
	printf "${ULINE}${BOLD}Value Meanings: \n\n${RESET}"
	for value in ${!BITS[@]}; do
		printf "${BOLD}${value}${RESET} = ${BITS[${value}]}\n"
	done
	__EXIT__
}

#Just prints out correct usage.
__USAGE__(){
	printf "${GREEN}\n./acmod.sh <Permission Code>. E.G. ./acmod 777.${RESET}"
	printf "${GREEN}\nCode must be 3 digits long with each digit from 0-7.\n${RESET}"
	__EXIT__
}

#Exits script.
__EXIT__(){
	printf "${YELLOW}\nQuitting...\n\n${RESET}"
	exit
}

__MAIN__

